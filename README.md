This is a port of the DLI/Course for GPU Memory Optimizations with CUDA C/C++.<br/>
<https://courses.nvidia.com/courses/course-v1:DLI+L-AC-02+V1/about>

Follow along with the instructions below or via python notebook
[TransposeOptimization.ipynb](src/TransposeOptimization.ipynb).

---

![](src/images/DLI%20Header.png)

# Introduction to Global and Shared Memory Optimizations

In this self-paced, hands-on lab, we will look at some simple but powerful
memory optimization techniques to improve performance of data access. These
memory optimizations are essential for obtaining good performance on GPU
applications. We'll step through a series of code examples and utilize the
[NVIDIA Visual Profiler](https://developer.nvidia.com/nvidia-visual-profiler)
to do guided performance optimizations to your code. This lab assumes that you
are familiar with introductory CUDA C/C++ topics and have exposure working with
threads, threadblocks, global memory and shared memory.

Lab created by Jonathan Bentz (follow [@jnbntz](https://twitter.com/jnbntz) on Twitter)


## Matrix Transpose

In this lab we're going to be programming a [matrix transpose](http://en.wikipedia.org/wiki/Transpose)
algorithm. For simplicity's sake we'll use square matrices and we'll use an
out-of-place algorithm. This will allow us to focus on the important memory
optimization techniques without worrying about corner cases and unevenly-shaped
matrices. We're going to be programming in CUDA C/C++ and we'll assume you have
a working knowledge of CUDA C/C++ syntax and concepts such as threads and blocks.

The matrix transpose algorithm is defined as *A*<sub>i,j</sub> = *B*<sub>i,j</sub>
where *A* and *B* are
![](https://latex.codecogs.com/gif.latex?%5Cinline%20M%20%5Ctimes%20M)
matrices and the subscripts *i,j* are the
row and column indices, respectively. (In the exercises today we'll be using
[column-major](http://en.wikipedia.org/wiki/Row-major_order#Column-major_order)
ordering of the elements.)

For example if you have a
![](https://latex.codecogs.com/gif.latex?%5Cinline%203%20%5Ctimes%203)
matrix *A* like the following

![](https://latex.codecogs.com/gif.latex?%5Cinline%20A%20%3D%20%5Cleft%28%20%5Cbegin%7Barray%7D%7Bccc%7D%20a%20%26%20d%20%26%20g%20%5C%5C%20b%20%26%20e%20%26%20h%20%5C%5C%20c%20%26%20f%20%26%20i%20%5Cend%7Barray%7D%20%5Cright%29)

then the transpose of the matrix, given by *A*<sup>T</sup> is

![](https://latex.codecogs.com/gif.latex?%5Cinline%20A%5E%7BT%7D%20%3D%20%5Cleft%28%20%5Cbegin%7Barray%7D%7Bccc%7D%20a%20%26%20b%20%26%20c%20%5C%5C%20d%20%26%20e%20%26%20f%20%5C%5C%20g%20%26%20h%20%26%20i%20%5Cend%7Barray%7D%20%5Cright%29)

This lab consists of three tasks that will require you to modify some code,
compile and execute it. For each task, a solution is provided so you can check
your work or take a peek at if you get lost.

If you are still confused now, or at any point in this lab, you can consult the
[FAQ](#FAQ) located at the bottom of this page.

## Error Checking

One of the most important programming techniques for writing robust code is
doing proper error checking. All CUDA runtime functions return an error code
of type **`cudaError_t`**. It is a good practice to check the error code
returned from all CUDA functions. In the code samples today, we've provided
two macros to help you do this. First, you can use `CUDA_CALL(F)` to wrap each
call you make to the CUDA runtime API. For example, instead of writing

```cpp
cudaMemcpy( h_c, c, sizeof(float), cudaMemcpyHostToDevice );
```

you would write

```cpp
CUDA_CALL( cudaMemcpy( h_c, c, sizeof(float), cudaMemcpyHostToDevice ) );
```

and this will check the return code of `cudaMemcpy` and tell you if there is an error.

There is an exception to this usage and that is when calling kernels. Kernels
do not return any value. To check whether a kernel was launched correctly you
can do the following. If you have a kernel launch

```cpp
kernel<<< 256, 256 >>>( d_a, d_b, d_c );
```

you would use the macro `CUDA_CHECK()` followed by
`CUDA_CALL( cudaDeviceSynchronize )` as below

```cpp
kernel<<< 256, 256 >>>( d_a, d_b, d_c );
CUDA_CHECK()
CUDA_CALL( cudaDeviceSynchronize() );`
```

In the error checking macros we've provided, if there is an error you'll get a
message printed to the screen and the program will terminate. If no errors are
detected the program will execute normally.

# Task #1 -- Naive Matrix Transpose

The first task you need to do is complete a naive transpose kernel. I call this
kernel naive because it is the simplest way to formulate the algorithm in
parallel. You should use 1 thread per matrix element. That is, each thread
reads from location (row,col) in the matrix and writes to location (col,row).
You'll use a 2D grid and 2D thread blocks. In the source code I've defined a
macro **INDX(row,col,LD)** which will help you translate between 2D matrix
coordinates into a 1D memory offset. INDX is defined as follows.

```cpp
#define INDX( row, col, ld ) ( ( (col) * (ld) ) + (row) )
```

For example if you want to access element (3,4) in a 10 by 10 matrix (using
0-based indexing), you'd use INDX(3,4,10) and this will give you the 1D offset
into the array, i.e., memory location 43.

In the code there are places where you'll find the text **FIXME**. This
indicates a place where you need to add code to complete the exercise. There
are helpful comments telling you what you need to add as well. When you are
finished you can compile and run your code as per the instructions that follow.
Your transpose kernel will be compared numerically against a host transpose for
functionality. You'll also be provided with a performance metric of bandwidth
in GB/s to show you how well your kernel is performing (higher numbers are
better). The program will output **PASS** or **FAIL** to indicate whether your
kernel is getting the correct answer.

Edit [task1.cu](src/task1-naive_transpose/task1-orig.cu) and compile.

```bash
# Execute this cell to compile the Hello Parallelism example labled task1
nvcc -lineinfo -DDEBUG -arch=sm_30 -o task1_out task1-naive_transpose/task1.cu && echo Compiled Successfully!
```

Once you see the words **Compiled Successfully!** from your compilation output,
run the program on the GPU node.

```bash
./task1_out
```

## Performance Profiling

Once you've gotten the code working properly from a functional perspective,
it's time to examine the performance profile of the kernel and see if we can
find any trouble spots and improve performance. We already know that this
kernel doesn't do anything interesting from a compute perspective, so it seems
logical that we'll be looking for ways to optimize the data movement in this algorithm.

We'll be using the NVIDIA Visual Profiler (NVVP) tool which comes standard with
the CUDA Toolkit software.

If you've never user NVVP before or if you want to read more about you can
[click here](https://developer.nvidia.com/nvidia-visual-profiler) for more information.

## Performance Profiling Task #1

Once the NVVP GUI starts, you should see something similar to the following screen shot.

<div align="center"><img src="src/images/nvvp1.png" width="60%" /></div>

Click *File -> New Session* to begin a new profiling session. You'll be
presented with a dialog box which allows you to setup the profiling session. In
the **File** Field, click the **Browse** button and navigate to your executable
file. Select **`task1_out`** and click **OK** in the bottom right corner. Then
click **Next** and then **Finish** and the profiler will begin by generating a
timeline of your executable. 

At this point you should be presented with something similar to the following screenshot.

<div align="center"><img src="src/images/nvvp2.png" width="60%" /></div>

We'll be using some of the guided inquiry features present in NVVP. On the left
side toward the bottom you'll see that the **Analysis** tab is selected. Here
is where we'll be spending a majority of our time. (Similar to most GUIs, you
can hover the mouse pointer over a barrier between sub-windows. A two-sided
arrow will appear and you can click/drag to make sub-windows of the profiler
larger or smaller to allow for more convenient viewing of the data.) 

At this point you should click **Examine GPU Usage** in the lower left corner.
This will run your program a few times to collect performance information. A
number of performance issues are identified but we really only care about
kernel performance. Next click **Examine Individual Kernels** and a list of all
the kernels are presented in the window to the right. Since we only have one
kernel, click the kernel name **naive_cuda_transpose** to highlight it. Once it's
highlighted, move back to the far left and click **Perform Kernel Analysis**.

In the **Results** pane you'll see a message that says **Kernel Performance is Bound By ...**.
It likely says that your kernel is bound by either memory bandwidth, or memory
latency. This is not surprising. Since we know we aren't doing any computation,
the only thing that could be limiting our performance is memory usage. Again on
the left, click the button **Perform Memory Bandwidth Analysis** (you may have
to scroll down to find this button). In the **Results** pane on the right, you
should see some performance data. In particular, look at the **Global Loads**
and **Global Stores** values. You should see that those values are not
equivalent. The number of transactions is *not* the same for the loads versus
stores. This is potentially a problem because you know that you're loading and
storing the same amount of data, i.e., you are loading the entire matrix and
then writing the entire matrix back to memory, albeit in transformed order. So
it stands to reason that the number of global loads and stores *should* be the
same, and yet they aren't. 

To understand this discrepancy we need to delve into a bit of detail about the
GPU memory system.

## GPU Global Memory System

In order utilize GPU device memory effeciently it is helpful to understand how
[global memory transactions](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#device-memory-accesses)
are executed by the GPU. Device memory is accessed via 32-, 64-, or 128-byte
memory transactions. These memory transactions must be naturally aligned: Only
the 32-, 64-, or 128-byte segments of device memory that are aligned to their
size (i.e., whose first address is a multiple of their size) can be read or
written by memory transactions. When a thread requests a data element (in our
case an 8-byte value) the memory system determines which memory segment your
data resides in and transfers the entire segment, whether the threads need the
entire segment or not. 

This is where the concept of coalescing memory transactions becomes important.
Since the amount of data transfered in a single transaction can be no smaller
than a memory segment, it makes sense for the threads to try and use most (or
all) of the data in that segment. A natural way to achieve this is for multiple
threads to request data that appears in the same segment. The easiest way to
accomplish this (and an excellent rule of thumb) is to maximize the "stride-1"
nature of your data accesses. That is, if thread N accesses memory location X,
then thread N+1 should access memory location X+1, etc. The GPU will coalesce
these memory requests into as few memory transactions as possible, thereby
taking the most advantage of the memory bandwith to and from global memory.

What happened in Task #1 is that we paid no attention to how memory was
accessed, so one of the accesses was completely coalesced (consecutive threads
accessed consecutive memory elements, that is, adjacent elements in the same
column) but one of the accesses was completely uncoalesced (consecutive threads
accessed memory elements with a stride equal to the number of rows in the
matrix, in our case 4096). Therefore we were moving a lot of wasted data that
we never used. 

# Task 2 -- Matrix Transpose with Shared Memory

When accessing global device memory the most important optimization is using
coalesced memory accesses as much as possible. It may seem like we have no
freedom to change the way that memory is accessed, but with a clever change to
the algorithm we can indeed ensure all the accesses to global memory are 
coalesced. To do this we'll need to use another layer of memory called
[shared memory](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#shared-memory). 

What we'll do to improve the performance will actually appear to be creating
more work for the GPU, but what we'll find is that we'll be utilizing the
memory system more efficiently. The way we'll achieve perfect coalescing is by
staging the global memory transfers through shared memory. 

In Task #1 we had each thread operate on a single element of the matrix. There
was no coordination between threads; each one did its own thing totally
independent from any other thread. In this task we'll have threads in the same
threadblock coordinate their efforts via shared memory. The granularity of the work in this algorithm is based more on threadblocks than individual threads.

The algorithm works as follows. We are already using 2D threadblocks of size 32
by 32. So let's partition the matrix into tiles of size 32 x 32 as well. Each
threadblock will read one 32 x 32 tile of the matrix into shared memory. Then
inside the shared memory the 32 x 32 tile will be transposed (there are no
coalescing requirements when reading/writing shared memory) and then the
threadblock will finally write out the 32 x 32 tile back to global memory. This
algorithm requires two transposes. We need to transpose the tile, i.e., if I
have tile (2,3) in global memory, I need to write it back to position (3,2). I
also need to transpose the 32 x 32 elements within the tile. 

You might be thinking that this algorithm is more complicated and seemingly
more work for the GPU than the first one, and in terms of lines-of-code or
complexity you would be correct. However, because we'll be using the GPU memory
system more efficiently we hope to get a performance improvement.

In the [code editor](src/task2-coalesce_smem/task2-orig.cu) add the correct
code to task2.cu to implement the transpose algorithm using shared memory and
achieving full coalescing of both reads and writes. Again recall that you need
to transpose the positions of the tiles, **AND** transpose the elements within
the tiles.

```bash
# Execute this cell to compile the example labled task2
nvcc -lineinfo -DDEBUG -arch=sm_30 -o task2_out task2-coalesce_smem/task2.cu && echo Compiled Successfully!
```


```bash
./task2_out
```

## Profiling with NVVP

Now that you have the code working if you compare your performance (GB/s) in
this Task to Task #1, you might actually get *worse* performance than you did
before. Let's profile this new version of the code to see what happened and if
we might be able to fix it.

If you have NVVP still open you can do *File -> New Session* to being profiling
this task, making sure to use **task2_out** as your new executable. 

Go through the same steps as above to get to the **Memory Bandwidth Analysis**.
Scroll down through the data to verify that **Global Loads** and
**Global Stores** are indeed the same now. This verifies that we've fixed our
global memory access patterns properly.

However you now have a message about shared memory alignment. Indeed if you
look at the transactions for **Shared Loads** and **Shared Stores** you see
they are not the same. Because we're reading/writing equal amounts of data to
shared memory, these values *ought* to be the same. The fact that they aren't
requires us to delve into a bit more detail about how shared memory works.

## Shared Memory

Earlier in this lab we briefly discussed shared memory and we said that it
doesn't require coalesced loads and stores to achieve optimal bandwidth. It
does have one property that we need to consider, namely that it is organized
into equal-sized [memory banks](http://docs.nvidia.com/cuda/cuda-c-best-practices-guide/index.html#shared-memory-and-memory-banks).
Knowing the structure of these banks is critical. If two threads in the same
warp attempt to access two memory locations that are served by the same bank,
it will result in something called a bank conflict and will require an
instruction replay. That is, the two threads can't both access the same bank
*at the same time* so one thread will have to wait while the first thread
accesses the bank. Then the second thread will take its turn accessing the
bank. All the while the entire warp cannot make progress until both threads'
shared memory accesses have been fulfilled.

## Shared Memory Bank Conflicts

When thinking about shared memory I like to think of a bank as a gate. Whenever
a thread wishes to access an element of shared memory, it must enter through a
gate (bank). There are thousands of shared memory locations but there are a
limited number of banks. To optimize our use of shared memory we need to
minimize (ideally eliminate) the times when multiple threads in the same warp
are trying to access memory locations in the same bank. Consider a simple example.

The figure below shows a 2d shared memory array of size 4-by-4. Assume that we
have a total of four banks in our memory system. The banks are labeled with
*0*, *1*, *2*, *3* and they are also color coded. The 4-by-4 array denotes a
shared memory array. The letters are the data elements in each memory location,
and the color signifies which bank that particular memory location is serviced
by. If you now have four threads trying to access data in the same row, i.e.,
data elements **A, B, C, D,** then you have no shared memory bank conflicts.
That is, **A, B, C, D** all have different colors for their banks so they all
reside in different banks. This means different threads can access those four
memory locations in the same transaction. But now imagine we want to access all
the elements in the same column, elements **A, E, I, M**. Clearly they all
reside in bank 0 (they are all green) so we'll have a four-way bank conflict.
This means that it will take the 4 threads roughly 4 times longer to access
this data. This is exactly the same access pattern we just coded above in
Task #2. We took an entire row and placed it into the entire column. One of our
accesses had great bank access, but the other had lots of bank conflicts. This
is the cause of the shared memory performance issue that we see in the Visual Profiler.

![caption](src/images/fig1.jpg)

## Fixing Bank Conflicts

It turns out that in our case, there's actually a pretty simple way to
alleviate the bank conflicts that we've encountered. We have to pad the shared
memory. We'll add an extra column to our shared memory array and this will fix
the bank conflicts that we have. To illustrate, take a look at the figure below.
Returning to our 4-by-4 matrix of data, notice again that the banks are labeled
on the top and color coded. Also notice that the data elements have colors
associated with them, i.e., their banks. Note that we've declared our array to
be of size 4-by-5, even though we still only have 16 data elements. Again
you see that if you have four threads accessing four elements in the same row
we have no bank conflicts, so that's good. But now if we have four threads
accessing four elements in the same column we also have no bank conflicts. That
is, elements **A, E, I, M** all reside in different banks now. It is the case
that we've wasted a bit of space in shared memory. The symbols **X** in the
figure show shared memory locations that are empty. But perhaps we can live
with this waste if the kernel shows significant performance improvement.

![caption](src/images/fig2.jpg)

# Task 3 -- Fixing Shared Memory Bank Conflicts

In the code editor below add the correct code to task3.cu to alleviate the
shared memory bank conflicts. GPUs of different generations have different
characteristics. Since we're using a GPU of
[compute capability 3.0](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#compute-capability-3-0),
the shared memory has [32 banks](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#shared-memory-3-0).
This time we haven't added any **FIXME** to the code because that's the entire
exercise, figuring out where and how to fix the shared memory bank conflicts!

Remember to **save** your code in the [editor](src/task3-smem_bank_conflicts/task3-orig.cu)
window before running/compiling.


```bash
# Execute this cell to compile the example labled task3
nvcc -lineinfo -DDEBUG -arch=sm_30 -o task3_out task3-smem_bank_conflicts/task3.cu && echo Compiled Successfully!
```


```bash
./task3_out
```

## Profiling with NVVP

How much better is your performance once you eliminate the bank conflicts?

As a final check on your work, profile your solution with NVVP and verify that
the shared memory bank conflicts have been reduced. Because we're using 64bit
memory types we can't fully remove all bank conflicts, but we can certainly
reduce them significantly.

## Summary

In this lab you have learned a few of the fundamental memory optimizations that
are critical to achieving good performance on GPU kernels. Just to recap;

* In Task #1 you wrote a naive transpose algorithm and after profiling it we
saw that we were not accessing global memory properly. To fix this,

* in Task #2 you used shared memory to stage the data transfer. This allowed us
to use coalesced memory accesses for both our reads and our writes to global
memory. We then profiled this kernel and found that we solved all our global
memory access problems, but introduced some shared memory access problems. We
learned that shared memory is organized into banks, and that eliminating bank
conflicts is key to good performance when using shared memory.

* In Task #3 you reduced the shared memory bank conflicts and found a
significant performance improvement. 

So you took a progression of steps to improve both the global and shared memory
access patterns. It cannot be stressed enough that using the best memory access
patterns is critical to achieving good GPU performance.

## Learn More

If you are interested in learning more, you can use the following resources:

* Learn more at the [CUDA Developer Zone](https://developer.nvidia.com/category/zone/cuda-zone).
* If you have an NVIDIA GPU in your system, you can download and install the [CUDA tookit](https://developer.nvidia.com/cuda-toolkit).
* Search or ask questions on [Stackoverflow](http://stackoverflow.com/questions/tagged/cuda) using the cuda tag

---
# <a name="FAQ"></a> Lab FAQ

Q: I'm encountering issues executing the cells, or other technical problems?<br>
A: Please see [this](https://developer.nvidia.com/self-paced-labs-faq#Troubleshooting) infrastructure FAQ.

Q: I'm getting unexpected behavior (i.e., incorrect output) when running any of
the tasks.<br>
A: It's possible that one or more of the CUDA Runtime API calls are actually
returning an error. Are you getting any errors printed to the screen about CUDA
Runtime errors?
