/*
 *  Copyright 2014 NVIDIA Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/*
 * Compile: nvcc -lineinfo -DDEBUG -arch=sm_30 -o task2_out task2/task2.cu && echo Compiled Successfully!
 */

/* PROFILE GLOBAL LOAD TRANSACTIONS

https://docs.nvidia.com/cuda/profiler-users-guide/index.html#nvprof-overview

$ nvprof \
  --metrics gld_transactions,gld_throughput,gst_transactions,gst_throughput,\
shared_load_transactions,shared_load_throughput,shared_store_transactions,shared_store_throughput \
  --events shared_ld_bank_conflict,shared_st_bank_conflict,shared_ld_transactions,shared_st_transactions \
  ./task2_out
Matrix size is 4096
==22240== NVPROF is profiling process 22240, command: ./task2_out
Total memory required per matrix is 134.217728 MB
Total time CPU is 0.322515 sec
Performance is 0.832320 GB/s
==22240== Some kernel(s) will be replayed on device 0 in order to collect all events/metrics.
Replaying kernel "smem_cuda_transpose(int, double const *, double*)" (done)
Total time GPU is 0.051939 sec
Performance is 5.168279 GB/sns
PASS
==22240== Profiling application: ./task2_out
==22240== Profiling result:
==22240== Event result:
Invocations                                Event Name         Min         Max         Avg       Total
Device "Tesla V100-PCIE-16GB (0)"
    Kernel: smem_cuda_transpose(int, double const *, double*)
          1                   shared_ld_bank_conflict      161374      161374      161374      161374
          1                   shared_st_bank_conflict    15842269    15842269    15842269    15842269
          1                    shared_ld_transactions     1211715     1211715     1211715     1211715
          1                    shared_st_transactions    16890897    16890897    16890897    16890897

==22240== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "Tesla V100-PCIE-16GB (0)"
    Kernel: smem_cuda_transpose(int, double const *, double*)
          1                          gld_transactions                  Global Load Transactions     4194304     4194304     4194304
          1                            gld_throughput                    Global Load Throughput  370.25GB/s  370.25GB/s  370.25GB/s
          1                          gst_transactions                 Global Store Transactions     4194304     4194304     4194304
          1                            gst_throughput                   Global Store Throughput  370.25GB/s  370.25GB/s  370.25GB/s
          1                  shared_load_transactions                  Shared Load Transactions     1211715     1211715     1211715
          1                    shared_load_throughput             Shared Memory Load Throughput  427.86GB/s  427.86GB/s  427.86GB/s
          1                 shared_store_transactions                 Shared Store Transactions    16890897    16890897    16890897
          1                   shared_store_throughput            Shared Memory Store Throughput  5964.2GB/s  5964.2GB/s  5964.2GB/s


EQUIVALENT IN NSIGHT-COMPUTE:
nv-nsight-cu-cli -f --section MemoryWorkloadAnalysis --details-all ./task2_out
nv-nsight-cu-cli -f --section MemoryWorkloadAnalysis --page raw ./task2_out

*/
#include <stdio.h>
#include <math.h>

#ifdef DEBUG
#define CUDA_CALL(F)  if( (F) != cudaSuccess ) \
  {printf("Error %s at %s:%d\n", cudaGetErrorString(cudaGetLastError()), \
   __FILE__,__LINE__); exit(-1);} 
#define CUDA_CHECK()  if( (cudaPeekAtLastError()) != cudaSuccess ) \
  {printf("Error %s at %s:%d\n", cudaGetErrorString(cudaGetLastError()), \
   __FILE__,__LINE__-1); exit(-1);} 
#else
#define CUDA_CALL(F) (F)
#define CUDA_CHECK() 
#endif

/* definitions of threadblock size in X and Y directions */

#define THREADS_PER_BLOCK_X 32
#define THREADS_PER_BLOCK_Y 32

/* definition of matrix linear dimension */

#define SIZE 4096

/* macro to index a 1D memory array with 2D indices in column-major order */

#define INDX( row, col, ld ) ( ( (col) * (ld) ) + (row) )

/* CUDA kernel for shared memory matrix transpose */

__global__ void smem_cuda_transpose(const int m, double const * const a,
    double * const c) {

    /* declare a shared memory array */

    // __shared__ double smemArray[FIXME][FIXME];
    __shared__ double smemArray[THREADS_PER_BLOCK_X][THREADS_PER_BLOCK_Y];

    /* determine my row and column indices for the error checking code */

    const int myRow = blockDim.x * blockIdx.x + threadIdx.x;
    const int myCol = blockDim.y * blockIdx.y + threadIdx.y;

    /* determine my row tile and column tile index */

    // const int tileX = FIXME
    // const int tileY = FIXME
    // HINT: The granularity of the work in this algorithm is based more on
    //     threadblocks than individual threads.
    const int tileX = blockDim.x * blockIdx.x;
    const int tileY = blockDim.y * blockIdx.y;

    if (myRow < m && myCol < m) {
        /* read to the shared mem array */
        /* HINT: threadIdx.x should appear somewhere in the first argument to */
        /* your INDX calculation for both a[] and c[].  This will ensure proper */
        /* coalescing. */

        // smemArray[FIXME][FIXME] = a[FIXME];
        smemArray[threadIdx.x][threadIdx.y] = a[INDX( myRow, myCol, m )];
//        smemArray[threadIdx.y][threadIdx.x] = a[INDX( myRow, myCol, m )];
    } /* end if */

    /* synchronize */
    __syncthreads();

    if (myRow < m && myCol < m) {
        /* write the result */
        // c[FIXME] = smemArray[FIXME][FIXME];
        // c[INDX( myCol, myRow, m )] = smemArray[threadIdx.x][threadIdx.y];
        c[INDX(tileY + threadIdx.x, tileX + threadIdx.y, m)] =
            smemArray[threadIdx.y][threadIdx.x];
//        c[INDX(tileY + threadIdx.x, tileX + threadIdx.y, m)] =
//            smemArray[threadIdx.x][threadIdx.y];
    } /* end if */
    return;

} /* end smem_cuda_transpose */

void host_transpose(const int m, double const * const a, double * const c) {

    /*
     *  naive matrix transpose goes here.
     */

    for (int j = 0; j < m; j++) {
        for (int i = 0; i < m; i++) {
            c[INDX(i, j, m)] = a[INDX(j, i, m)];
        } /* end for i */
    } /* end for j */

} /* end host_dgemm */

int main(int argc, char *argv[]) {

    int size = SIZE;

    fprintf(stdout, "Matrix size is %d\n", size);

    /* declaring pointers for array */

    double *h_a, *h_c;
    double *d_a, *d_c;

    size_t numbytes = (size_t) size * (size_t) size * sizeof(double);

    /* allocating host memory */

    h_a = (double *) malloc(numbytes);
    if (h_a == NULL) {
        fprintf(stderr, "Error in host malloc h_a\n");
        return 911;
    }

    h_c = (double *) malloc(numbytes);
    if (h_c == NULL) {
        fprintf(stderr, "Error in host malloc h_c\n");
        return 911;
    }

    /* allocating device memory */

    CUDA_CALL(cudaMalloc((void** ) &d_a, numbytes));
    CUDA_CALL(cudaMalloc((void** ) &d_c, numbytes));

    /* set result matrices to zero */

    memset(h_c, 0, numbytes);
    CUDA_CALL(cudaMemset(d_c, 0, numbytes));

    fprintf( stdout, "Total memory required per matrix is %lf MB\n",
        (double) numbytes / 1000000.0);

    /* initialize input matrix with random value */

    for (int i = 0; i < size * size; i++) {
        h_a[i] = double(rand()) / (double(RAND_MAX) + 1.0);
    } /* end for */

    /* copy input matrix from host to device */

    CUDA_CALL(cudaMemcpy(d_a, h_a, numbytes, cudaMemcpyHostToDevice));

    /* create and start timer */

    cudaEvent_t start, stop;
    CUDA_CALL(cudaEventCreate(&start));
    CUDA_CALL(cudaEventCreate(&stop));
    CUDA_CALL(cudaEventRecord(start, 0));

    /* call naive cpu transpose function */

    host_transpose(size, h_a, h_c);

    /* stop CPU timer */

    CUDA_CALL(cudaEventRecord(stop, 0));
    CUDA_CALL(cudaEventSynchronize(stop));
    float elapsedTime;
    CUDA_CALL(cudaEventElapsedTime(&elapsedTime, start, stop));

    /* print CPU timing information */

    fprintf(stdout, "Total time CPU is %f sec\n", elapsedTime / 1000.0f);
    fprintf(stdout, "Performance is %f GB/s\n",
        8.0 * 2.0 * (double) size * (double) size
            / ((double) elapsedTime / 1000.0) * 1.e-9);

    /* setup threadblock size and grid sizes */

    dim3 threads( THREADS_PER_BLOCK_X, THREADS_PER_BLOCK_Y, 1);
    dim3 blocks((size / THREADS_PER_BLOCK_X) + 1,
        (size / THREADS_PER_BLOCK_Y) + 1, 1);

    /* start timers */
    CUDA_CALL(cudaEventRecord(start, 0));

    /* call smem GPU transpose kernel */

    smem_cuda_transpose<<< blocks, threads >>>( size, d_a, d_c );
    CUDA_CHECK();
    CUDA_CALL(cudaDeviceSynchronize());

    /* stop the timers */

    CUDA_CALL(cudaEventRecord(stop, 0));
    CUDA_CALL(cudaEventSynchronize(stop));
    CUDA_CALL(cudaEventElapsedTime(&elapsedTime, start, stop));

    /* print GPU timing information */

    fprintf(stdout, "Total time GPU is %f sec\n", elapsedTime / 1000.0f);
    fprintf(stdout, "Performance is %f GB/s\n",
        8.0 * 2.0 * (double) size * (double) size
            / ((double) elapsedTime / 1000.0) * 1.e-9);

    /* copy data from device to host */

    CUDA_CALL(cudaMemset(d_a, 0, numbytes));
    CUDA_CALL(cudaMemcpy(h_a, d_c, numbytes, cudaMemcpyDeviceToHost));

    /* compare GPU to CPU for correctness */

    for (int j = 0; j < size; j++) {
        for (int i = 0; i < size; i++) {
            if (h_c[INDX(i, j, size)] != h_a[INDX(i, j, size)]) {
                printf("Error in element %d,%d\n", i, j);
                printf("Host %f, device %f\n", h_c[INDX(i, j, size)],
                    h_a[INDX(i, j, size)]);
                printf("FAIL\n");
                goto end;
            }
        } /* end for i */
    } /* end for j */

    /* free the memory */
    printf("PASS\n");

    end: free(h_a);
    free(h_c);
    CUDA_CALL(cudaFree(d_a));
    CUDA_CALL(cudaFree(d_c));

    CUDA_CALL(cudaDeviceReset());

    return 0;
}
