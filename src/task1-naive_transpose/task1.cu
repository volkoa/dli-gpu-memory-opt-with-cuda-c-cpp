/*
 *  Copyright 2014 NVIDIA Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/*
 * Compile: nvcc -lineinfo -DDEBUG -arch=sm_30 -o task1_out task1/task1.cu && echo Compiled Successfully!
 */

/*
https://docs.nvidia.com/cuda/profiler-users-guide/index.html#nvprof-overview

$ nvprof -m gld_transactions,gld_throughput,gst_transactions,gst_throughput ./task1_out
Matrix size is 4096
==22632== NVPROF is profiling process 22632, command: ./task1_out
Total memory required per matrix is 134.217728 MB
Total time CPU is 0.321762 sec
Performance is 0.834268 GB/s
==22632== Some kernel(s) will be replayed on device 0 in order to collect all events/metrics.
Replaying kernel "naive_cuda_transpose(int, double const *, double*)" (done)
Total time GPU is 0.038801 sec
Performance is 6.918183 GB/s
PASS
==22632== Profiling application: ./task1_out
==22632== Profiling result:
==22632== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "Tesla V100-PCIE-16GB (0)"
    Kernel: naive_cuda_transpose(int, double const *, double*)
          1                          gld_transactions                  Global Load Transactions    16777216    16777216    16777216
          1                            gld_throughput                    Global Load Throughput  1520.9GB/s  1520.9GB/s  1520.9GB/s
          1                          gst_transactions                 Global Store Transactions     4194304     4194304     4194304
          1                            gst_throughput                   Global Store Throughput  380.22GB/s  380.22GB/s  380.22GB/s


EQUIVALENT WITH NSIGHT-COMPUTE:
nv-nsight-cu-cli -f --section MemoryWorkloadAnalysis --details-all ./task1_out
nv-nsight-cu-cli -f --section MemoryWorkloadAnalysis --page raw ./task1_out

*/
#include <stdio.h>

#ifdef DEBUG
#define CUDA_CALL(F)  if( (F) != cudaSuccess ) \
  {printf("Error %s at %s:%d\n", cudaGetErrorString(cudaGetLastError()), \
   __FILE__,__LINE__); exit(-1);} 
#define CUDA_CHECK()  if( (cudaPeekAtLastError()) != cudaSuccess ) \
  {printf("Error %s at %s:%d\n", cudaGetErrorString(cudaGetLastError()), \
   __FILE__,__LINE__-1); exit(-1);} 
#else
#define CUDA_CALL(F) (F)
#define CUDA_CHECK() 
#endif

/* definitions of threadblock size in X and Y directions */

#define THREADS_PER_BLOCK_X 32
#define THREADS_PER_BLOCK_Y 32

/* definition of matrix linear dimension */

#define SIZE 4096

/* macro to index a 1D memory array with 2D indices in column-major order */

#define INDX( row, col, ld ) ( ( (col) * (ld) ) + (row) )

/* CUDA kernel for naive matrix transpose */

__global__ void naive_cuda_transpose(const int m, const double * const a,
    double * const c) {
    //const int myRow = FIXME
    //const int myCol = FIXME
    const int myRow = threadIdx.x + blockIdx.x * blockDim.x;
    const int myCol = threadIdx.y + blockIdx.y * blockDim.y;

    if (myRow < m && myCol < m) {
        // c[FIXME] = a[FIXME];
        c[INDX(myRow, myCol, m)] = a[INDX(myCol, myRow, m)];
    } /* end if */
    return;

} /* end naive_cuda_transpose */

void host_transpose(const int m, const double * const a, double *c) {

    /*
     *  naive matrix transpose goes here.
     */

    for (int j = 0; j < m; j++) {
        for (int i = 0; i < m; i++) {
            c[INDX(i, j, m)] = a[INDX(j, i, m)];
        } /* end for i */
    } /* end for j */

} /* end host_dgemm */

int main(int argc, char *argv[]) {

    int size = SIZE;

    fprintf(stdout, "Matrix size is %d\n", size);

    /* declaring pointers for array */

    double *h_a, *h_c;
    double *d_a, *d_c;

    size_t numbytes = (size_t) size * (size_t) size * sizeof(double);

    /* allocating host memory */

    h_a = (double *) malloc(numbytes);
    if (h_a == NULL) {
        fprintf(stderr, "Error in host malloc h_a\n");
        return 911;
    }

    h_c = (double *) malloc(numbytes);
    if (h_c == NULL) {
        fprintf(stderr, "Error in host malloc h_c\n");
        return 911;
    }

    /* allocating device memory */

    CUDA_CALL(cudaMalloc((void** ) &d_a, numbytes));
    CUDA_CALL(cudaMalloc((void** ) &d_c, numbytes));

    /* set result matrices to zero */

    memset(h_c, 0, numbytes);
    CUDA_CALL(cudaMemset(d_c, 0, numbytes));

    fprintf( stdout, "Total memory required per matrix is %lf MB\n",
        (double) numbytes / 1000000.0);

    /* initialize input matrix with random value */

    for (int i = 0; i < size * size; i++) {
        h_a[i] = double(rand()) / (double(RAND_MAX) + 1.0);
    }

    /* copy input matrix from host to device */

    CUDA_CALL(cudaMemcpy(d_a, h_a, numbytes, cudaMemcpyHostToDevice));

    /* create and start timer */

    cudaEvent_t start, stop;
    CUDA_CALL(cudaEventCreate(&start));
    CUDA_CALL(cudaEventCreate(&stop));
    CUDA_CALL(cudaEventRecord(start, 0));

    /* call naive cpu transpose function */

    host_transpose(size, h_a, h_c);

    /* stop CPU timer */

    CUDA_CALL(cudaEventRecord(stop, 0));
    CUDA_CALL(cudaEventSynchronize(stop));
    float elapsedTime;
    CUDA_CALL(cudaEventElapsedTime(&elapsedTime, start, stop));

    /* print CPU timing information */

    fprintf(stdout, "Total time CPU is %f sec\n", elapsedTime / 1000.0f);
    fprintf(stdout, "Performance is %f GB/s\n",
        8.0 * 2.0 * (double) size * (double) size
            / ((double) elapsedTime / 1000.0) * 1.e-9);

    /* setup threadblock size and grid sizes */

    dim3 threads( THREADS_PER_BLOCK_X, THREADS_PER_BLOCK_Y, 1);
    size_t nblocks_x = (size + threads.x - 1) / threads.x;
    size_t nblocks_y = (size + threads.y - 1) / threads.y;
    // dim3 blocks(FIXME, FIXME, 1);
    dim3 blocks(nblocks_x, nblocks_y, 1);

    /* start timers */
    CUDA_CALL(cudaEventRecord(start, 0));

    /* call naive GPU transpose kernel */

    naive_cuda_transpose<<< blocks, threads >>>( size, d_a, d_c );
    CUDA_CHECK()
    CUDA_CALL(cudaDeviceSynchronize());

    /* stop the timers */

    CUDA_CALL(cudaEventRecord(stop, 0));
    CUDA_CALL(cudaEventSynchronize(stop));
    CUDA_CALL(cudaEventElapsedTime(&elapsedTime, start, stop));

    /* print GPU timing information */

    fprintf(stdout, "Total time GPU is %f sec\n", elapsedTime / 1000.0f);
    fprintf(stdout, "Performance is %f GB/s\n",
        8.0 * 2.0 * (double) size * (double) size
            / ((double) elapsedTime / 1000.0) * 1.e-9);

    /* copy data from device to host */

    CUDA_CALL(cudaMemset(d_a, 0, numbytes));
    CUDA_CALL(cudaMemcpy(h_a, d_c, numbytes, cudaMemcpyDeviceToHost));

    /* compare GPU to CPU for correctness */

    for (int j = 0; j < size; j++) {
        for (int i = 0; i < size; i++) {
            if (h_c[INDX(i, j, size)] != h_a[INDX(i, j, size)]) {
                printf("Error in element %d,%d\n", i, j);
                printf("Host %f, device %f\n", h_c[INDX(i, j, size)],
                    h_a[INDX(i, j, size)]);
                printf("FAIL\n");
                goto end;
            } /* end fi */
        } /* end for i */
    } /* end for j */

    /* free the memory */
    printf("PASS\n");

    end: free(h_a);
    free(h_c);
    CUDA_CALL(cudaFree(d_a));
    CUDA_CALL(cudaFree(d_c));
    CUDA_CALL(cudaDeviceReset());

    return 0;
} /* end main */
