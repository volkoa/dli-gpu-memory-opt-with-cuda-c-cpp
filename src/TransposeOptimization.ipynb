{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![](images/DLI%20Header.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Global and Shared Memory Optimizations\n",
    "\n",
    "In this self-paced, hands-on lab, we will look at some simple but powerful memory optimization techniques to improve performance of data access.  These memory optimizations are essential for obtaining good performance on GPU applications.  We'll step through a series of code examples and utilize the [NVIDIA Visual Profiler](https://developer.nvidia.com/nvidia-visual-profiler) to do guided performance optimizations to your code.  This lab assumes that you are familiar with introductory CUDA C/C++ topics and have exposure working with threads, threadblocks, global memory and shared memory.\n",
    "\n",
    "Lab created by Jonathan Bentz (follow [@jnbntz](https://twitter.com/jnbntz) on Twitter)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Matrix Transpose\n",
    "\n",
    "In this lab we're going to be programming a [matrix transpose](http://en.wikipedia.org/wiki/Transpose) algorithm.  For simplicity's sake we'll use square matrices and we'll use an out-of-place algorithm.  This will allow us to focus on the important memory optimization techniques without worrying about corner cases and unevenly-shaped matrices.  We're going to be programming in CUDA C/C++ and we'll assume you have a working knowledge of CUDA C/C++ syntax and concepts such as threads and blocks.\n",
    "\n",
    "The matrix transpose algorithm is defined as $A_{i,j} = B_{j,i}$ where $A$ and $B$ are $M \\times M$ matrices and the subscripts $i,j$ are the row and column indices, respectively.  (In the exercises today we'll be using [column-major](http://en.wikipedia.org/wiki/Row-major_order#Column-major_order) ordering of the elements.)\n",
    "\n",
    "For example if you have a $3 \\times 3$ matrix $A$ like the following $$A = \\left( \\begin{array}{ccc}\n",
    "a & d & g \\\\\n",
    "b & e & h \\\\\n",
    "c & f & i \\end{array} \\right),$$\n",
    "then the transpose of the matrix, given by $A^{T}$ is\n",
    "$$A^{T} = \\left( \\begin{array}{ccc}\n",
    "a & b & c \\\\\n",
    "d & e & f \\\\\n",
    "g & h & i \\end{array} \\right).$$\n",
    "\n",
    "This lab consists of three tasks that will require you to modify some code, compile and execute it.  For each task, a solution is provided so you can check your work or take a peek at if you get lost.\n",
    "\n",
    "If you are still confused now, or at any point in this lab, you can consult the <a href=\"#FAQ\">FAQ</a> located at the bottom of this page."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Error Checking\n",
    "\n",
    "One of the most important programming techniques for writing robust code is doing proper error checking.  All CUDA runtime functions return an error code of type **`cudaError_t`**.  It is a good practice to check the error code returned from all CUDA functions.  In the code samples today, we've provided two macros to help you do this.  First, you can use `CUDA_CALL(F)` to wrap each call you make to the CUDA runtime API.  For example, instead of writing\n",
    "\n",
    "```cpp\n",
    "cudaMemcpy( h_c, c, sizeof(float), cudaMemcpyHostToDevice );\n",
    "```\n",
    "\n",
    "you would write\n",
    "\n",
    "```cpp\n",
    "CUDA_CALL( cudaMemcpy( h_c, c, sizeof(float), cudaMemcpyHostToDevice ) );\n",
    "```\n",
    "\n",
    "and this will check the return code of `cudaMemcpy` and tell you if there is an error.  \n",
    "\n",
    "There is an exception to this usage and that is when calling kernels.  Kernels do not return any value.  To check whether a kernel was launched correctly you can do the following.  If you have a kernel launch\n",
    "\n",
    "```cpp\n",
    "kernel<<< 256, 256 >>>( d_a, d_b, d_c );\n",
    "```\n",
    "\n",
    "you would use the macro `CUDA_CHECK()` followed by `CUDA_CALL( cudaDeviceSynchronize )` as below\n",
    "\n",
    "```cpp\n",
    "kernel<<< 256, 256 >>>( d_a, d_b, d_c );\n",
    "CUDA_CHECK()\n",
    "CUDA_CALL( cudaDeviceSynchronize() );`\n",
    "```\n",
    "\n",
    "In the error checking macros we've provided, if there is an error you'll get a message printed to the screen and the program will terminate.  If no errors are detected the program will execute normally."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Task #1 -- Naive Matrix Transpose\n",
    "\n",
    "The first task you need to do is complete a naive transpose kernel.  I call this kernel naive because it is the simplest way to formulate the algorithm in parallel.  You should use 1 thread per matrix element.  That is, each thread reads from location (row,col) in the matrix and writes to location (col,row).  You'll use a 2D grid and 2D thread blocks.  In the source code I've defined a macro **INDX(row,col,LD)** which will help you translate between 2D matrix coordinates into a 1D memory offset.  INDX is defined as follows.\n",
    "\n",
    "```cpp\n",
    "#define INDX( row, col, ld ) ( ( (col) * (ld) ) + (row) )\n",
    "```\n",
    "\n",
    "For example if you want to access element (3,4) in a 10 by 10 matrix (using 0-based indexing), you'd use INDX(3,4,10) and this will give you the 1D offset into the array, i.e., memory location 43.  \n",
    "\n",
    "In the code there are places where you'll find the text **FIXME**.  This indicates a place where you need to add code to complete the exercise.  There are helpful comments telling you what you need to add as well.  When you are finished you can compile and run your code as per the instructions that follow.  Your transpose kernel will be compared numerically against a host transpose for functionality.  You'll also be provided with a performance metric of bandwidth in GB/s to show you how well your kernel is performing (higher numbers are better).  The program will output **PASS** or **FAIL** to indicate whether your kernel is getting the correct answer.\n",
    "\n",
    "In the [code editor](task1-naive_transpose/task1-orig.cu) be sure to click the **save** button before compiling your code so that you are compiling/running your most recent code changes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To compile your code, execute the cell block below by giving it focus (clicking on it with your mouse), and hitting Ctrl-Enter, or pressing the play button in the toolbar above.  If all goes well, you should see get some output returned below the grey cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Execute this cell to compile the Hello Parallelism example labled task1\n",
    "!nvcc -lineinfo -DDEBUG -arch=sm_30 -o task1_out task1/task1.cu && echo Compiled Successfully!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you see the words **Compiled Successfully!** from your compilation output, execute the cell below to run the program on the GPU node."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!./task1_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Performance Profiling\n",
    "\n",
    "Once you've gotten the code working properly from a functional perspective, it's time to examine the performance profile of the kernel and see if we can find any trouble spots and improve performance.  We already know that this kernel doesn't do anything interesting from a compute perspective, so it seems logical that we'll be looking for ways to optimize the data movement in this algorithm.\n",
    "\n",
    "We'll be using the NVIDIA Visual Profiler (NVVP) tool which comes standard with the CUDA Toolkit software.\n",
    "\n",
    "If you've never user NVVP before or if you want to read more about you can [click here](https://developer.nvidia.com/nvidia-visual-profiler) for more information."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Performance Profiling Task #1\n",
    "\n",
    "Once the NVVP GUI starts, you should see something similar to the following screen shot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src=\"images/nvvp1.png\" width=\"60%\" /></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Click *File -> New Session* to begin a new profiling session.  You'll be presented with a dialog box which allows you to setup the profiling session.  In the **File** Field, click the **Browse** button and navigate to your executable file. Select **`task1_out`** and click **OK** in the bottom right corner.  Then click **Next** and then **Finish** and the profiler will begin by generating a timeline of your executable.  \n",
    "\n",
    "At this point you should be presented with something similar to the following screenshot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src=\"images/nvvp2.png\" width=\"60%\" /></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll be using some of the guided inquiry features present in NVVP.  On the left side toward the bottom you'll see that the **Analysis** tab is selected.  Here is where we'll be spending a majority of our time.  (Similar to most GUIs, you can hover the mouse pointer over a barrier between sub-windows.  A two-sided arrow will appear and you can click/drag to make sub-windows of the profiler larger or smaller to allow for more convenient viewing of the data.) \n",
    "\n",
    "At this point you should click **Examine GPU Usage** in the lower left corner.  This will run your program a few times to collect performance information.  A number of performance issues are identified but we really only care about kernel performance.  Next click **Examine Individual Kernels** and a list of all the kernels are presented in the window to the right.  Since we only have one kernel, click the kernel name **naive_cuda_transpose** to highlight it.  Once it's highlighted, move back to the far left and click **Perform Kernel Analysis**.\n",
    "\n",
    "In the **Results** pane you'll see a message that says **Kernel Performance is Bound By ...**.  It likely says that your kernel is bound by either memory bandwidth, or memory latency.  This is not surprising.  Since we know we aren't doing any computation, the only thing that could be limiting our performance is memory usage.  Again on the left, click the button **Perform Memory Bandwidth Analysis** (you may have to scroll down to find this button).  In the **Results** pane on the right, you should see some performance data.  In particular, look at the **Global Loads** and **Global Stores** values.  You should see that those values are not equivalent.  The number of transactions is *not* the same for the loads versus stores.  This is potentially a problem because you know that you're loading and storing the same amount of data, i.e., you are loading the entire matrix and then writing the entire matrix back to memory, albeit in transformed order.  So it stands to reason that the number of global loads and stores *should* be the same, and yet they aren't.  \n",
    "\n",
    "To understand this discrepancy we need to delve into a bit of detail about the GPU memory system."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## GPU Global Memory System\n",
    "\n",
    "In order utilize GPU device memory effeciently it is helpful to understand how [global memory transactions](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#device-memory-accesses) are executed by the GPU.  Device memory is accessed via 32-, 64-, or 128-byte memory transactions. These memory transactions must be naturally aligned: Only the 32-, 64-, or 128-byte segments of device memory that are aligned to their size (i.e., whose first address is a multiple of their size) can be read or written by memory transactions.  When a thread requests a data element (in our case an 8-byte value) the memory system determines which memory segment your data resides in and transfers the entire segment, whether the threads need the entire segment or not.  \n",
    "\n",
    "This is where the concept of coalescing memory transactions becomes important.  Since the amount of data transfered in a single transaction can be no smaller than a memory segment, it makes sense for the threads to try and use most (or all) of the data in that segment.  A natural way to achieve this is for multiple threads to request data that appears in the same segment.  The easiest way to accomplish this (and an excellent rule of thumb) is to maximize the \"stride-1\" nature of your data accesses.  That is, if thread N accesses memory location X, then thread N+1 should access memory location X+1, etc.  The GPU will coalesce these memory requests into as few memory transactions as possible, thereby taking the most advantage of the memory bandwith to and from global memory.\n",
    "\n",
    "What happened in Task #1 is that we paid no attention to how memory was accessed, so one of the accesses was completely coalesced (consecutive threads accessed consecutive memory elements, that is, adjacent elements in the same column) but one of the accesses was completely uncoalesced (consecutive threads accessed memory elements with a stride equal to the number of rows in the matrix, in our case 4096).  Therefore we were moving a lot of wasted data that we never used.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Task 2 -- Matrix Transpose with Shared Memory\n",
    "\n",
    "When accessing global device memory the most important optimization is using coalesced memory accesses as much as possible.  It may seem like we have no freedom to change the way that memory is accessed, but with a clever change to the algorithm we can indeed ensure all the accesses to global memory are coalesced.  To do this we'll need to use another layer of memory called [shared memory](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#shared-memory).  \n",
    "\n",
    "What we'll do to improve the performance will actually appear to be creating more work for the GPU, but what we'll find is that we'll be utilizing the memory system more efficiently.  The way we'll achieve perfect coalescing is by staging the global memory transfers through shared memory.  \n",
    "\n",
    "In Task #1 we had each thread operate on a single element of the matrix.  There was no coordination between threads; each one did its own thing totally independent from any other thread.  In this task we'll have threads in the same threadblock coordinate their efforts via shared memory.  The granularity of the work in this algorithm is based more on threadblocks than individual threads.\n",
    "\n",
    "The algorithm works as follows.  We are already using 2D threadblocks of size 32 by 32.  So let's partition the matrix into tiles of size 32 x 32 as well.  Each threadblock will read one 32 x 32 tile of the matrix into shared memory.  Then inside the shared memory the 32 x 32 tile will be transposed (there are no coalescing requirements when reading/writing shared memory) and then the threadblock will finally write out the 32 x 32 tile back to global memory.  This algorithm requires two transposes.  We need to transpose the tile, i.e., if I have tile (2,3) in global memory, I need to write it back to position (3,2).  I also need to transpose the 32 x 32 elements within the tile.  \n",
    "\n",
    "You might be thinking that this algorithm is more complicated and seemingly more work for the GPU than the first one, and in terms of lines-of-code or complexity you would be correct.  However, because we'll be using the GPU memory system more efficiently we hope to get a performance improvement.\n",
    "\n",
    "In the [code editor](task2-coalesce_smem/task2-orig.cu) add the correct code to task2.cu to implement the transpose algorithm using shared memory and achieving full coalescing of both reads and writes.  Again recall that you need to transpose the positions of the tiles, **AND** transpose the elements within the tiles.\n",
    "\n",
    "Remember to **save** your code in the editor window before compiling/running."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Execute this cell to compile the example labled task2\n",
    "!nvcc -lineinfo -DDEBUG -arch=sm_30 -o task2_out task2/task2.cu && echo Compiled Successfully!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!./task2_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Profiling with NVVP\n",
    "\n",
    "Now that you have the code working if you compare your performance (GB/s) in this Task to Task #1, you might actually get *worse* performance than you did before.  Let's profile this new version of the code to see what happened and if we might be able to fix it.\n",
    "\n",
    "If you have NVVP still open you can do *File -> New Session* to being profiling this task, making sure to use **gpudev1/notebook/task2_out** as your new executable.  \n",
    "\n",
    "Go through the same steps as above to get to the **Memory Bandwidth Analysis**.  Scroll down through the data to verify that **Global Loads** and **Global Stores** are indeed the same now.  This verifies that we've fixed our global memory access patterns properly.\n",
    "\n",
    "However you now have a message about shared memory alignment.  Indeed if you look at the transactions for **Shared Loads** and **Shared Stores** you see they are not the same.  Because we're reading/writing equal amounts of data to shared memory, these values *ought* to be the same.  The fact that they aren't requires us to delve into a bit more detail about how shared memory works."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Shared Memory\n",
    "\n",
    "Earlier in this lab we briefly discussed shared memory and we said that it doesn't require coalesced loads and stores to achieve optimal bandwidth.  It does have one property that we need to consider, namely that it is organized into equal-sized [memory banks](http://docs.nvidia.com/cuda/cuda-c-best-practices-guide/index.html#shared-memory-and-memory-banks).  Knowing the structure of these banks is critical.  If two threads in the same warp attempt to access two memory locations that are served by the same bank, it will result in something called a bank conflict and will require an instruction replay.  That is, the two threads can't both access the same bank *at the same time* so one thread will have to wait while the first thread accesses the bank.  Then the second thread will take its turn accessing the bank.  All the while the entire warp cannot make progress until both threads' shared memory accesses have been fulfilled."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Shared Memory Bank Conflicts\n",
    "\n",
    "When thinking about shared memory I like to think of a bank as a gate.  Whenever a thread wishes to access an element of shared memory, it must enter through a gate (bank).  There are thousands of shared memory locations but there are a limited number of banks.  To optimize our use of shared memory we need to minimize (ideally eliminate) the times when multiple threads in the same warp are trying to access memory locations in the same bank.  Consider a simple example.  \n",
    "\n",
    "The figure below shows a 2d shared memory array of size $4 \\times 4$.  Assume that we have a total of four banks in our memory system.  The banks are labeled with $0, 1, 2, 3$ and they are also color coded.  The $4 \\times 4$ array denotes a shared memory array.  The letters are the data elements in each memory location, and the color signifies which bank that particular memory location is serviced by.  If you now have four threads trying to access data in the same row, i.e., data elements $A, B, C, D$, then you have no shared memory bank conflicts.  That is, $A, B, C, D$ all have different colors for their banks so they all reside in different banks.  This means different threads can access those four memory locations in the same transaction.  But now imagine we want to access all the elements in the same column, elements $A, E, I, M$.  Clearly they all reside in bank 0 (they are all green) so we'll have a four-way bank conflict.  This means that it will take the 4 threads roughly 4 times longer to access this data.  This is exactly the same access pattern we just coded above in Task #2.  We took an entire row and placed it into the entire column.  One of our accesses had great bank access, but the other had lots of bank conflicts.  This is the cause of the shared memory performance issue that we see in the Visual Profiler."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![caption](images/fig1.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fixing Bank Conflicts\n",
    "\n",
    "It turns out that in our case, there's actually a pretty simple way to alleviate the bank conflicts that we've encountered.  We have to pad the shared memory.  We'll add an extra column to our shared memory array and this will fix the bank conflicts that we have.  To illustrate, take a look at the figure below.  Returning to our $4 \\times 4$ matrix of data, notice again that the banks are labeled on the top and color coded.  Also notice that the data elements have colors associated with them, i.e., their banks.  Note that we've declared our array to be of size $4 \\times 5$, even though we still only have 16 data elements.  Again you see that if you have four threads accessing four elements in the same row we have no bank conflicts, so that's good.  But now if we have four threads accessing four elements in the same column we also have no bank conflicts.  That is, elements $A, E, I, M$ all reside in different banks now.  It is the case that we've wasted a bit of space in shared memory.  The symbols $X$ in the figure show shared memory locations that are empty.  But perhaps we can live with this waste if the kernel shows significant performance improvement. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![caption](images/fig2.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Task 3 -- Fixing Shared Memory Bank Conflicts\n",
    "\n",
    "In the code editor below add the correct code to task3.cu to alleviate the shared memory bank conflicts.  GPUs of different generations have different characteristics.  Since we're using a GPU of [compute capability 3.0](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#compute-capability-3-0), the shared memory has [32 banks](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#shared-memory-3-0).  This time we haven't added any **FIXME** to the code because that's the entire exercise, figuring out where and how to fix the shared memory bank conflicts!\n",
    "\n",
    "Remember to **save** your code in the [editor](task3-smem_bank_conflicts/task3-orig.cu) window before running/compiling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Execute this cell to compile the example labled task3\n",
    "!nvcc -lineinfo -DDEBUG -arch=sm_30 -o task3_out task3/task3.cu && echo Compiled Successfully!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "!./task3_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Profiling with NVVP\n",
    "\n",
    "How much better is your performance once you eliminate the bank conflicts?\n",
    "\n",
    "As a final check on your work, profile your solution with NVVP and verify that the shared memory bank conflicts have been reduced.  Because we're using 64bit memory types we can't fully remove all bank conflicts, but we can certainly reduce them significantly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary\n",
    "\n",
    "In this lab you have learned a few of the fundamental memory optimizations that are critical to achieving good performance on GPU kernels.  Just to recap;\n",
    "\n",
    "* In Task #1 you wrote a naive transpose algorithm and after profiling it we saw that we were not accessing global memory properly.  To fix this,\n",
    "* in Task #2 you used shared memory to stage the data transfer.  This allowed us to use coalesced memory accesses for both our reads and our writes to global memory.  We then profiled this kernel and found that we solved all our global memory access problems, but introduced some shared memory access problems.  We learned that shared memory is organized into banks, and that eliminating bank conflicts is key to good performance when using shared memory.\n",
    "* In Task #3 you reduced the shared memory bank conflicts and found a significant performance improvement.  \n",
    "\n",
    "So you took a progression of steps to improve both the global and shared memory access patterns.  It cannot be stressed enough that using the best memory access patterns is critical to achieving good GPU performance."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Learn More\n",
    "\n",
    "If you are interested in learning more, you can use the following resources:\n",
    "\n",
    "* Learn more at the [CUDA Developer Zone](https://developer.nvidia.com/category/zone/cuda-zone).\n",
    "* If you have an NVIDIA GPU in your system, you can download and install the [CUDA tookit](https://developer.nvidia.com/cuda-toolkit).\n",
    "* Search or ask questions on [Stackoverflow](http://stackoverflow.com/questions/tagged/cuda) using the cuda tag"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"FAQ\"></a>\n",
    "---\n",
    "# Lab FAQ\n",
    "\n",
    "Q: I'm encountering issues executing the cells, or other technical problems?<br>\n",
    "A: Please see [this](https://developer.nvidia.com/self-paced-labs-faq#Troubleshooting) infrastructure FAQ.\n",
    "\n",
    "Q: I'm getting unexpected behavior (i.e., incorrect output) when running any of the tasks.<br>\n",
    "A: It's possible that one or more of the CUDA Runtime API calls are actually returning an error.  Are you getting any errors printed to the screen about CUDA Runtime errors?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<style>\n",
    "p.hint_trigger{\n",
    "  margin-bottom:7px;\n",
    "  margin-top:-5px;\n",
    "  background:#64E84D;\n",
    "}\n",
    ".toggle_container{\n",
    "  margin-bottom:0px;\n",
    "}\n",
    ".toggle_container p{\n",
    "  margin:2px;\n",
    "}\n",
    ".toggle_container{\n",
    "  background:#f0f0f0;\n",
    "  clear: both;\n",
    "  font-size:100%;\n",
    "}\n",
    "</style>\n",
    "<script>\n",
    "$(\"p.hint_trigger\").click(function(){\n",
    "   $(this).toggleClass(\"active\").next().slideToggle(\"normal\");\n",
    "});\n",
    "   \n",
    "$(\".toggle_container\").hide();\n",
    "</script>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
